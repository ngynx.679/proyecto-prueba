import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class FormularioBase extends StatefulWidget {
 @override
 _FormularioBaseState createState() => _FormularioBaseState();
}
class _FormularioBaseState extends State<FormularioBase> {

//key del form para la validacion...
 GlobalKey<FormState> keyForm = new GlobalKey();
 //instancias para nuestros campos de txt
 TextEditingController  nameCtrl = new TextEditingController();
 TextEditingController  emailCtrl = new TextEditingController();
 TextEditingController  mobileCtrl = new TextEditingController();
 TextEditingController  passwordCtrl = new TextEditingController();
 TextEditingController  repeatPassCtrl = new TextEditingController();

 //test
 bool _obscureText = true;
 void _toggle() {
      setState(() {
        _obscureText = !_obscureText;
      });
}


 @override
 Widget build(BuildContext context) {
   return MaterialApp(
     home: new Scaffold(
       appBar: new AppBar(
         title: new Text('Form Base'),
         backgroundColor: Colors.deepPurple,
       ),
       body: new SingleChildScrollView(
         child: new Container(
           margin: new EdgeInsets.all(34.0),
           child: new Form(
             key: keyForm,
             child: formUI(),
           ),
         ),
       ),
     ),
   );
 }

 fieldsFom(icon, item) {
   return Padding(
     padding: EdgeInsets.symmetric(vertical: 6),
     child: Card(child: ListTile(leading: Icon(icon), title: item)),
   );
 }


 Widget formUI() {
   return  Column(
     children: <Widget>[
       fieldsFom(
           Icons.person_outline,
           TextFormField(
             controller: nameCtrl,
             decoration: new InputDecoration(
               labelText: 'Nombre',
               fillColor: Colors.amber
             ),
             validator: validateName,
           )
          ),
       fieldsFom(
           Icons.phone,
           TextFormField(
             controller: mobileCtrl,
               decoration: new InputDecoration(
                 labelText: 'Numero de telefono',
               ),
               keyboardType: TextInputType.phone,
               maxLength: 9,
               validator: validateMobile,
            )
          ),
       
       fieldsFom(
           Icons.email,
           TextFormField(
             controller: emailCtrl,
               decoration: new InputDecoration(
                 labelText: 'Email',
               ),
               keyboardType: TextInputType.emailAddress,
               maxLength: 32,
               validator: validateEmail,
            )
          ),
       fieldsFom(
           Icons.remove_red_eye,
           TextFormField(
             controller: passwordCtrl,
             decoration: InputDecoration(
               labelText: 'Contraseña',
             ),
             validator: validateName,
             obscureText: _obscureText,
           ),
           
          ),
//Cambiar por un radio button o implementarle un icono
       new FlatButton(
        onPressed: _toggle,
        child: new Text(_obscureText ? "Show" : "Hide")
       ),
       
       fieldsFom(
           Icons.remove_red_eye,
           TextFormField(
             controller: repeatPassCtrl,
             obscureText: _obscureText,
             decoration: InputDecoration(
               labelText: 'Repetir Contraseña',
             ),
             validator: validateRepeatPassword,
           )
          ),
       
//Boton SAVE 
        GestureDetector(
            onTap: (){
              save();
            },
            child: Container(
                      margin: new EdgeInsets.all(30.0),
                      alignment: Alignment.center,
                      decoration: ShapeDecoration(
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(30.0)),
                        gradient: LinearGradient(colors: [
                          Color(0xCC6633AC),
                          Color(0xFF03A0FE),
                        ],
                        begin: Alignment.topLeft, end: Alignment.bottomRight),
                      ),
                        child: Text("SAVE",
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 18,
                                fontWeight: FontWeight.w500)),
                        padding: EdgeInsets.only(top: 16, bottom: 16),
                    ),
            ),

     ],
   );
 }

String validateName(String value) {
   String pattern = r'(^[a-zA-Z ]*$)';
   RegExp regExp = new RegExp(pattern);
   if (value.length == 0) {
     return "El nombre es necesario";
   } else if (!regExp.hasMatch(value)) {
     return "El nombre debe de ser a-z y A-Z";
   }
   return null;
 }

 String validateMobile(String value) {
   //String patttern = r'(^[0-9]*$)';
   //RegExp regExp = new RegExp(patttern);
   if (value.length == 0) {
     return "Este campo es obligatorio!";
   } else if (value.length != 9) {
     return "El numero debe tener 9 digitos";
   }
   return null;
 }

 String validateEmail(String value) {
   //REGEX email
   String pattern =
       r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
   RegExp regExp = new RegExp(pattern);
   if (value.length == 0) {
     return "El correo es obligatorio!";
   } else if (!regExp.hasMatch(value)) {
     return "Correo invalido";
   } else {
     return null;
   }
 }

 String validatePassword(String value) {
   String pattern = r'^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[*.!@$%^&(){}[]:;<>,.?/~_+-=|\]).{8,32}$';
   RegExp regExp = new RegExp(pattern);
   if (value.length == 0) {
     return "La contraseña es obligatoria!";
   } else {
     return null;
   }
   /*
   else if (!regExp.hasMatch(value)) {
     return "La contraseña debe tener 9 caracteres ";
   } */
 }

 String validateRepeatPassword(String value) {
   //print("valor $value passsword ${passwordCtrl.text}");
   
   if (value.length == 0) {
     return "Las contraseñas no coinciden";
   }
   else if (value != passwordCtrl.text) {
     return "Las contraseñas no coinciden";
   }
   return null;
 }

//metodo para poder continuar con la validacion ... utilizando el keyform inicialmente generado
 save() {
   if (keyForm.currentState.validate()) {
     print("Nombre ${nameCtrl.text}");
     print("Telefono ${mobileCtrl.text}");
     print("Correo ${emailCtrl.text}");
     print("Contraseña ${passwordCtrl.text}");
// limpiar los campos ...
         keyForm.currentState.reset();
   }
 }
}


